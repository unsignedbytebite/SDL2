#include <SDL.h>

int main(int argc, char *args[])
{
	// Create window 
	SDL_Window* window = SDL_CreateWindow(
		"Example01", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);

	// Create renderer for window
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	// Our green square location
	int x = 0, y = 0;

	// Main doRender loop
	SDL_Event event;
	bool doRender = true;
	while (doRender)
	{
		// Handle events on queue
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
				// Stop SDL if the window is closed
			case SDL_QUIT:
				doRender = false;
				break;

				// Check for if a key is pressed
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
					// If ESCAPE is pressed then stop
				case SDLK_ESCAPE:
					doRender = false;
					break;
				case SDLK_a:
					x -= 4;
					break;
				case SDLK_d:
					x += 4;
					break;
				case SDLK_s:
					y += 4;
					break;
				case SDLK_w:
					y -= 4;
					break;
				}
				break;
			}
		}

		// doRender to screen

		// Clear back buffer cornflower blue
		SDL_SetRenderDrawColor(renderer, 0x64, 0x95, 0xED, 0xFF);
		SDL_RenderClear(renderer);

		// Render a filled box from the middle point to the rest of the window
		SDL_SetRenderDrawColor(renderer, 0xff, 0x22, 0x11, 0xFF);
		const SDL_Rect drawRect = { 400, 300, 400, 300 };
		SDL_RenderFillRect(renderer, &drawRect);

		// Render lines to each corner to the window
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0xFF); // White
		SDL_RenderDrawLine(renderer, 0, 0, 800, 600);
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF); // Black
		SDL_RenderDrawLine(renderer, 0, 600, 800, 0);

		// Render our green square
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
		const SDL_Rect greenRect = { x, y, 32, 32 };
		SDL_RenderFillRect(renderer, &greenRect);

		// Swap back buffer to front
		SDL_RenderPresent(renderer);

	}

	// Release SDL
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();

	return 0;
}