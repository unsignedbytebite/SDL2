﻿#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <vector>

class FrameSet
{
public:
	FrameSet(const unsigned short delay = 4) : mDelay(delay)
	{}
	~FrameSet()
	{}

	// Add a frame to the set
	void add(const SDL_Rect frame)
	{
		mFrames.push_back(frame);
	}

	// Process an update step
	void FrameSet::step()
	{
		++mFrameCount;

		// If the framecounter is greater than the delay then step
		if (mFrameCount >= mDelay)
		{
			mFrameCount = 0;

			//Go to next frame
			++mFrameHead;

			// If frame head is greater than the set size
			if (mFrameHead > mFrames.size() - 1)
			{
				mFrameHead = 0;
			}
		}
	}

	// Get the current frame
	SDL_Rect* FrameSet::current()
	{
		return &mFrames.at(mFrameHead);
	}

protected:
	// Frames in the set
	std::vector<SDL_Rect> mFrames;

	// Play counters
	unsigned short mFrameCount, mFrameHead, mDelay;
};

int main(int argc, char *args[])
{

	#pragma region SDL_start
	// Create window 
	SDL_Window* window = SDL_CreateWindow(
		"Example02", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);

	// Create renderer for window
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	// Initialize PNG 
	IMG_Init(IMG_INIT_PNG);

	// Initialize TrueType 
	TTF_Init();
	#pragma endregion

	#pragma region Texture
	// Load image at specified path as a surface
	SDL_Surface* loadedSurface = IMG_Load("assets\\DoomCharMarine.png");

	// Create texture from surface pixels
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);

	// Get image dimensions
	const int textureWidth = loadedSurface->w;
	const int textureHeight = loadedSurface->h;

	// Get rid of old loaded surface
	SDL_FreeSurface(loadedSurface);

	// Create the frame sets/cells for animation
	std::vector<FrameSet> sets;
	const int framesX = textureWidth / 4;
	const int framesY = textureHeight / 4;

	{
		FrameSet set{ 8 };
		set.add(SDL_Rect{ framesX * 0, framesY * 0, framesX, framesY });
		set.add(SDL_Rect{ framesX * 1, framesY * 0, framesX, framesY });
		set.add(SDL_Rect{ framesX * 2, framesY * 0, framesX, framesY });
		set.add(SDL_Rect{ framesX * 3, framesY * 0, framesX, framesY });
		sets.push_back(set);
	}

	{
		FrameSet set{ 8 };
		set.add(SDL_Rect{ framesX * 0, framesY * 1, framesX, framesY });
		set.add(SDL_Rect{ framesX * 1, framesY * 1, framesX, framesY });
		set.add(SDL_Rect{ framesX * 2, framesY * 1, framesX, framesY });
		set.add(SDL_Rect{ framesX * 3, framesY * 1, framesX, framesY });
		sets.push_back(set);
	}

	{
		FrameSet set{ 8 };
		set.add(SDL_Rect{ framesX * 0, framesY * 2, framesX, framesY });
		set.add(SDL_Rect{ framesX * 1, framesY * 2, framesX, framesY });
		set.add(SDL_Rect{ framesX * 2, framesY * 2, framesX, framesY });
		set.add(SDL_Rect{ framesX * 3, framesY * 2, framesX, framesY });
		sets.push_back(set);
	}

	{
		FrameSet set{ 8 };
		set.add(SDL_Rect{ framesX * 0, framesY * 3, framesX, framesY });
		set.add(SDL_Rect{ framesX * 1, framesY * 3, framesX, framesY });
		set.add(SDL_Rect{ framesX * 2, framesY * 3, framesX, framesY });
		set.add(SDL_Rect{ framesX * 3, framesY * 3, framesX, framesY });
		sets.push_back(set);
	}
	#pragma endregion

	#pragma region Font_texture
	// Load the font
	TTF_Font* font = TTF_OpenFont("assets\\comic.ttf", 32);

	// Create the sprite font
	SDL_Surface* textSurface = TTF_RenderText_Solid(font, "The quick brown fox jumps over the lazy dog", SDL_Color{ 255, 0, 255 });

	//Create texture from surface pixels
	SDL_Texture* fontTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

	//Get image dimensions
	const int fontTextureWidth = textSurface->w;
	const int fontTextureHeight = textSurface->h;

	//Get rid of old surface
	SDL_FreeSurface(textSurface);
	#pragma endregion

	#pragma region Font_texture_unicode
	// Load the font
	TTF_Font* fontUnicode = TTF_OpenFont("assets\\simhei.ttf", 32);

	// Create the sprite font
	Uint16 textUnicode[] = { L'你', L'是', L'我', L'的', L'中', L'文', L'老', L'师', L'吗', L'。', '\0' };
	SDL_Surface* textSurfaceUnicode = TTF_RenderUNICODE_Blended(fontUnicode, textUnicode, SDL_Color{ 255, 150, 100 });

	//Create texture from surface pixels
	SDL_Texture* fontTextureUnicode = SDL_CreateTextureFromSurface(renderer, textSurfaceUnicode);

	//Get image dimensions
	const int fontTextureWidthUnicode = textSurfaceUnicode->w;
	const int fontTextureHeightUnicode = textSurfaceUnicode->h;

	//Get rid of old surface
	SDL_FreeSurface(textSurfaceUnicode);
	#pragma endregion

	// Main doRender loop
	SDL_Event event;
	bool doRender = true;
	while (doRender)
	{
		#pragma region Events
		// Handle events on queue
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
				// Stop SDL if the window is closed
			case SDL_QUIT:
			doRender = false;
			break;

			// Check for if a key is pressed
			case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
				// If ESCAPE is pressed then stop
			case SDLK_ESCAPE:
			doRender = false;
			break;
			}
			break;
			}
		}
		#pragma endregion

		#pragma region Render
		// Render to screen

		// Clear back buffer 
		SDL_SetRenderDrawColor(renderer, 0x09, 0x09, 0x09, 0xFF);
		SDL_RenderClear(renderer);

		// Draw the texture
		{
			const SDL_Rect clip{ 0, 0, textureWidth, textureHeight };
			SDL_RenderCopyEx(renderer, texture, NULL, &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
		}

		// Draw animated frames
		{
			for (unsigned char i = 0; i < sets.size(); ++i)
			{
				sets[i].step();
				const SDL_Rect clip{ textureWidth + 16, i * sets[i].current()->h, sets[i].current()->w, sets[i].current()->h };
				SDL_RenderCopyEx(renderer, texture, sets[i].current(), &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
			}
		}

		// Draw all animated frames rotating
		{
			for (unsigned char i = 0; i < sets.size(); ++i)
			{
				const SDL_Rect clip{ textureWidth + 16 + sets[i].current()->h, i * sets[i].current()->h, sets[i].current()->w, sets[i].current()->h };
				SDL_RenderCopyEx(renderer, texture, sets[i].current(), &clip, i * 45, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
			}
		}

		// Draw the font texture
		{
			const SDL_Rect clip{ 0, textureHeight + 16, fontTextureWidth, fontTextureHeight };
			SDL_RenderCopyEx(renderer, fontTexture, NULL, &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
		}

		// Draw the fontUnicode  texture
		{
			const SDL_Rect clip{ 0, textureHeight + fontTextureHeight + 16, fontTextureWidthUnicode , fontTextureHeightUnicode };
			SDL_RenderCopyEx(renderer, fontTextureUnicode, NULL, &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
		}

		// Swap back buffer to front
		SDL_RenderPresent(renderer);
		#pragma endregion
	}

	#pragma region Free
	// Free
	SDL_DestroyTexture(fontTexture);
	SDL_DestroyTexture(fontTextureUnicode);
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
	#pragma endregion

	return 0;
}